package com.example.qlnhanvien;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME_NV = "nhanvien";
    public static final String TABLE_NAME_PHONG_BAN = "phongban";
    public static final String TABLE_NAME_PB_NV = "phongban_nv";

    // Table columns
    public static final String _ID = "_id";
    public static final String _TEN = "ten";
    public static final String _QUEQUAN = "quequan";
    public static final String _NGAYSINH = "ngaysinh";
    public static final String _MOTA = "mota";
    public static final String _ID_NV = "id_nv";
    public static final String _ID_PB = "id_pb";

    // Database Information
    static final String DB_NAME = "qlnv.DB";

    // database version
    static final int DB_VERSION = 1;

    // Creating table query
    private static final String CREATE_TABLE_NV = "create table " + TABLE_NAME_NV + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + _TEN + " TEXT NOT NULL, "
            + _NGAYSINH + " TEXT , "
            + _ID_PB + " INTEGER , "
            + _QUEQUAN + " TEXT   );";
    private static final String CREATE_TABLE_PB = "create table " + TABLE_NAME_PHONG_BAN + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + _TEN + " TEXT NOT NULL, "
            + _MOTA + " TEXT   );";
    private static final String CREATE_TABLE_PB_NV = "create table " + TABLE_NAME_PB_NV + "(" + _ID_NV
            + " INTEGER , "
            + _ID_PB + " INTEGER  );";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_NV);
        db.execSQL(CREATE_TABLE_PB);
        db.execSQL(CREATE_TABLE_PB_NV);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_NV);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PHONG_BAN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PB_NV);
        onCreate(db);
    }
}
