package com.example.qlnhanvien.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnhanvien.R;
import com.example.qlnhanvien.model.NhanVien;
import com.example.qlnhanvien.model.PhongBan;

import java.util.ArrayList;

public class PhongBanAdapter extends RecyclerView.Adapter<PhongBanAdapter.ViewHolder> {
    private ArrayList<PhongBan> listdata;
    private CallBack callBack;

    public PhongBanAdapter(ArrayList<PhongBan> listdata, CallBack callBack) {
        this.listdata = listdata;
        this.callBack = callBack;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_phong_ban, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PhongBan p = listdata.get(position);
        holder.id_pb.setText(String.format(holder.itemView.getResources().getString(R.string.id), p.getMa()+""));
        holder.ten_pb.setText(String.format(holder.itemView.getResources().getString(R.string.ten), p.getTen()+""));
        holder.mota_pb.setText(String.format(holder.itemView.getResources().getString(R.string.mota), p.getMota()+""));
        holder.textViewOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onClickViewOptions(holder.textViewOptions, p);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onClickItem(p);
            }
        });
    }

    public void updateList(ArrayList<PhongBan> list) {
        if (listdata != null && !listdata.isEmpty()) {
            listdata.clear();
        }
        if (list != null && !list.isEmpty()) {
            listdata.addAll(list);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mota_pb, ten_pb, id_pb, textViewOptions;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mota_pb = itemView.findViewById(R.id.mota_pb);
            this.ten_pb = itemView.findViewById(R.id.ten_pb);
            this.id_pb = itemView.findViewById(R.id.id_pb);
            this.textViewOptions = itemView.findViewById(R.id.textViewOptions);
        }
    }

    public interface CallBack {
        void onClickViewOptions(TextView view, PhongBan phongBan);
        void onClickItem ( PhongBan phongBan);
    }
}