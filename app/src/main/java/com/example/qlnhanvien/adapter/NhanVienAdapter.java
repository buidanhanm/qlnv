package com.example.qlnhanvien.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnhanvien.R;
import com.example.qlnhanvien.model.NhanVien;

import java.util.ArrayList;

public class NhanVienAdapter extends RecyclerView.Adapter<NhanVienAdapter.ViewHolder> {
    private ArrayList<NhanVien> listdata;
    private CallBack callBack;
    private boolean isShowEdit = true;

    public NhanVienAdapter(ArrayList<NhanVien> listdata, CallBack callBack) {
        this.listdata = listdata;
        this.callBack = callBack;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_nv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NhanVien nhanVien = listdata.get(position);
        holder.id_nv.setText(String.format(holder.itemView.getResources().getString(R.string.id), nhanVien.getMa() + ""));
        holder.ten_nv.setText(String.format(holder.itemView.getResources().getString(R.string.ten), nhanVien.getTen() + ""));
        holder.ngaysinh.setText(String.format(holder.itemView.getResources().getString(R.string.ngaysinh), nhanVien.getNgaysinh() + ""));
        holder.que_quan.setText(String.format(holder.itemView.getResources().getString(R.string.quenquan), nhanVien.getQuequan() + ""));
        if (isShowEdit) {
            holder.textViewOptions.setVisibility(View.VISIBLE);
            holder.tv_add_nv_pb.setVisibility(View.GONE);
        } else {
            holder.textViewOptions.setVisibility(View.GONE);
            holder.tv_add_nv_pb.setVisibility(View.VISIBLE);
        }
        holder.textViewOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onClickViewOptions(holder.textViewOptions, nhanVien);
            }
        });
        holder.tv_add_nv_pb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onClickViewOptions(holder.textViewOptions, nhanVien);
            }
        });
    }

    public void updateList(ArrayList<NhanVien> list) {
        if (listdata != null && !listdata.isEmpty()) {
            listdata.clear();
        }
        if (list != null && !list.isEmpty()) {
            listdata.addAll(list);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public void setShowEdit(boolean isShowEdit) {
        this.isShowEdit = isShowEdit;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView ngaysinh, que_quan, ten_nv, id_nv, textViewOptions;
        public Button tv_add_nv_pb;

        public ViewHolder(View itemView) {
            super(itemView);
            this.ngaysinh = itemView.findViewById(R.id.ngaysinh);
            this.que_quan = itemView.findViewById(R.id.que_quan);
            this.ten_nv = itemView.findViewById(R.id.ten_nv);
            this.id_nv = itemView.findViewById(R.id.id_nv);
            this.textViewOptions = itemView.findViewById(R.id.textViewOptions);
            this.tv_add_nv_pb = itemView.findViewById(R.id.tv_add_nv_pb);
        }
    }

    public interface CallBack {
        void onClickViewOptions(TextView view, NhanVien nhanVien);
    }
}