package com.example.qlnhanvien;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnhanvien.adapter.NhanVienAdapter;
import com.example.qlnhanvien.adapter.PhongBanAdapter;
import com.example.qlnhanvien.model.NhanVien;
import com.example.qlnhanvien.model.PhongBan;

import java.util.ArrayList;

public class ListPhongBanActivity extends AppCompatActivity implements PhongBanAdapter.CallBack {

    private DBManager dbManager;
    private RecyclerView recyclerView;
    private PhongBanAdapter phongBanAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.list_phongban);
        setTitle("Danh sách Phòng ban");
        findViewById(R.id.btn_add_pb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListPhongBanActivity.this, AddEditPhongBanActivity.class);
                intent.putExtra("IS_EDIT", false);
                startActivity(intent);
            }
        });
        dbManager = new DBManager(this);
        dbManager.open();

        ArrayList listPhongBan = dbManager.getListPhongBan();
        recyclerView = (RecyclerView) findViewById(R.id.rvPhongBan);
        phongBanAdapter = new PhongBanAdapter(listPhongBan, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(phongBanAdapter);
    }

    @Override
    public void onClickViewOptions(TextView view, final PhongBan phongban) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.inflate(R.menu.main);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        dbManager.deletePhongBan(phongban.getMa());
                        updateListPB();
                        return true;
                    case R.id.edit:
                        Intent intent = new Intent(ListPhongBanActivity.this, AddEditPhongBanActivity.class);
                        intent.putExtra("IS_EDIT", true);
                        intent.putExtra("ID", phongban.getMa());
                        intent.putExtra("MO_TA", phongban.getMota());
                        intent.putExtra("TEN", phongban.getTen());
                        startActivity(intent);
                        return true;
                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }

    @Override
    public void onClickItem(PhongBan phongBan) {
        Intent intent = new Intent(ListPhongBanActivity.this, ListPhongBanNhanVienActivity.class);
        intent.putExtra("PB", phongBan);
        startActivity(intent);
    }

    void updateListPB() {
        ArrayList phongBans = dbManager.getListPhongBan();
        phongBanAdapter.updateList(phongBans);
    }
}