package com.example.qlnhanvien;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnhanvien.adapter.NhanVienAdapter;
import com.example.qlnhanvien.model.NhanVien;

import java.util.ArrayList;

public class ListNhanVienActivity extends AppCompatActivity implements NhanVienAdapter.CallBack {

    private DBManager dbManager;
    private RecyclerView recyclerView;
    private NhanVienAdapter nhanVienAdapter;
    private boolean isAddNV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.list_nv);
        isAddNV = getIntent().getBooleanExtra("IS_ADD_NV", false);

        ImageView btn_add_nv = findViewById(R.id.btn_add_nv);
        btn_add_nv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListNhanVienActivity.this, AddEditNhanVienActivity.class);
                intent.putExtra("IS_EDIT", false);
                startActivity(intent);
            }
        });
        if (isAddNV) {
            setTitle("Thêm nhân viên vào phòng ban");
            btn_add_nv.setVisibility(View.GONE);
        } else {
            setTitle("Danh sách Nhân Viên");
            btn_add_nv.setVisibility(View.VISIBLE);
        }
        dbManager = new DBManager(this);
        dbManager.open();

        ArrayList nhanViens;
        if (isAddNV) {
            nhanViens = dbManager.getListNhanVienByOtherIdPhongBan();
        } else {
            nhanViens = dbManager.getListNhanVien();
        }
        recyclerView = (RecyclerView) findViewById(R.id.rvNhanVien);
        nhanVienAdapter = new NhanVienAdapter(nhanViens, this);
        nhanVienAdapter.setShowEdit(!isAddNV);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(nhanVienAdapter);
    }

    @Override
    public void onClickViewOptions(TextView view, final NhanVien nhanVien) {
        if (isAddNV) {
            int idPB = getIntent().getIntExtra("ID_PB", 0);
            nhanVien.setIdPb(idPB);
            dbManager.updateNV(nhanVien.getMa(), nhanVien);
            finish();
        } else {
            PopupMenu popup = new PopupMenu(this, view);
            popup.inflate(R.menu.main);
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.delete:
                            dbManager.deleteNhanVien(nhanVien.getMa());
                            updateListNv();
                            return true;
                        case R.id.edit:
                            Intent intent = new Intent(ListNhanVienActivity.this, AddEditNhanVienActivity.class);
                            intent.putExtra("IS_EDIT", true);
                            intent.putExtra("ID", nhanVien.getMa());
                            intent.putExtra("QUE_QUAN", nhanVien.getQuequan());
                            intent.putExtra("NGAY_SINH", nhanVien.getNgaysinh());
                            intent.putExtra("TEN", nhanVien.getTen());
                            startActivity(intent);
                            return true;
                        default:
                            return false;
                    }
                }
            });
            //displaying the popup
            popup.show();
        }

    }

    void updateListNv() {
        ArrayList nhanViens = dbManager.getListNhanVien();
        nhanVienAdapter.updateList(nhanViens);
    }
}