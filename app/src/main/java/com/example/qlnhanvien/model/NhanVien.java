package com.example.qlnhanvien.model;

public class NhanVien {
    int ma;
    String ten;
    String quequan;
    String ngaysinh;

    public int getIdPb() {
        return idPb;
    }

    public void setIdPb(int idPb) {
        this.idPb = idPb;
    }

    int idPb;

    public NhanVien() {
    }

    public NhanVien(String ten, String quequan, String ngaysinh) {
        this.ten = ten;
        this.quequan = quequan;
        this.ngaysinh = ngaysinh;
    }

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getQuequan() {
        return quequan;
    }

    public void setQuequan(String quequan) {
        this.quequan = quequan;
    }

    public String getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(String ngaysinh) {
        this.ngaysinh = ngaysinh;
    }
}
