package com.example.qlnhanvien.model;

import java.io.Serializable;

public class PhongBan implements Serializable {
    int ma;
    String ten;
    String mota;

    public PhongBan() {
    }

    public PhongBan(String ten, String mota) {
        this.ten = ten;
        this.mota = mota;
    }

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }
}
