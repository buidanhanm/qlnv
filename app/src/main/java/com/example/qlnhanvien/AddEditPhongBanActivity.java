package com.example.qlnhanvien;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.qlnhanvien.model.NhanVien;
import com.example.qlnhanvien.model.PhongBan;

public class AddEditPhongBanActivity extends AppCompatActivity implements OnClickListener {

    private Button addTodoBtn;
    private EditText editText_ten;
    private EditText edt_mota;

    private DBManager dbManager;
    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addedit_pb);
        editText_ten = (EditText) findViewById(R.id.edt_ten);
        edt_mota = (EditText) findViewById(R.id.edt_mota);
        addTodoBtn = (Button) findViewById(R.id.action_pb);
        isEdit = getIntent().getBooleanExtra("IS_EDIT", false);
        if (isEdit) {
            setTitle("Chỉnh sửa Phòng ban");
            editText_ten.setText(getIntent().getStringExtra("TEN"));
            edt_mota.setText(getIntent().getStringExtra("MO_TA"));
            addTodoBtn.setText("Chỉnh sửa Phòng ban");
        } else {
            setTitle("Thêm Phòng ban");
            addTodoBtn.setText("Thêm Phòng ban");
        }

        dbManager = new DBManager(this);
        dbManager.open();
        addTodoBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.action_pb) {
            final String ten = editText_ten.getText().toString();
            final String mota = edt_mota.getText().toString();
            if (isEdit) {
                dbManager.updatePhongBan(getIntent().getIntExtra("ID", -1), new PhongBan(ten, mota));
            } else {
                dbManager.insertPhongBan(new PhongBan(ten, mota));
            }
            Intent main = new Intent(AddEditPhongBanActivity.this, ListPhongBanActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(main);
        }
    }

}