package com.example.qlnhanvien;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.qlnhanvien.model.NhanVien;
import com.example.qlnhanvien.model.PhongBan;

import java.util.ArrayList;

public class DBManager {

    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insertNhanVien(NhanVien nhanVien) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper._TEN, nhanVien.getTen());
        contentValue.put(DatabaseHelper._QUEQUAN, nhanVien.getQuequan());
        contentValue.put(DatabaseHelper._NGAYSINH, nhanVien.getNgaysinh());
        database.insert(DatabaseHelper.TABLE_NAME_NV, null, contentValue);
    }

    public void insertPhongBan(PhongBan phongBan) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper._TEN, phongBan.getTen());
        contentValue.put(DatabaseHelper._MOTA, phongBan.getMota());
        database.insert(DatabaseHelper.TABLE_NAME_PHONG_BAN, null, contentValue);
    }

    public void insertPhongBanNV(int idPB, int idNv) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper._ID_NV, idNv);
        contentValue.put(DatabaseHelper._ID_PB, idPB);
        database.insert(DatabaseHelper.TABLE_NAME_PB_NV, null, contentValue);
    }

    public ArrayList<NhanVien> getListNhanVien() {
        String[] columns = new String[]{DatabaseHelper._ID, DatabaseHelper._TEN, DatabaseHelper._QUEQUAN, DatabaseHelper._NGAYSINH,DatabaseHelper._ID_PB};
        Cursor mCursor = database.query(DatabaseHelper.TABLE_NAME_NV, columns, null, null, null, null, null);
        ArrayList<NhanVien> nhanViens = new ArrayList<>();
        for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            NhanVien nhanVien = new NhanVien();
            int id = mCursor.getInt(mCursor.getColumnIndex(DatabaseHelper._ID));
            int idPB = mCursor.getInt(mCursor.getColumnIndex(DatabaseHelper._ID_PB));
            String ten = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper._TEN));
            String quequan = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper._QUEQUAN));
            String ngaysinh = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper._NGAYSINH));
            nhanVien.setMa(id);
            nhanVien.setIdPb(idPB);
            nhanVien.setNgaysinh(ngaysinh);
            nhanVien.setQuequan(quequan);
            nhanVien.setTen(ten);
            nhanViens.add(nhanVien);
        }
        return nhanViens;
    }

    public ArrayList<NhanVien> getListNhanVienByIdPhongBan(int idPb) {
        ArrayList<NhanVien> nhanViens =   getListNhanVien();
        ArrayList<NhanVien> nhanViensResult  =  new ArrayList<>();
        for (NhanVien nhanVien : nhanViens){
            if(nhanVien.getIdPb() == idPb){
                nhanViensResult.add(nhanVien);
            }
        }
        return nhanViensResult;
    }
    public ArrayList<NhanVien> getListNhanVienByOtherIdPhongBan() {
        ArrayList<NhanVien> nhanViens =   getListNhanVien();
        ArrayList<NhanVien> nhanViensResult  =  new ArrayList<>();
        for (NhanVien nhanVien : nhanViens){
            if(nhanVien.getIdPb() == 0){
                nhanViensResult.add(nhanVien);
            }
        }
        return nhanViensResult;
    }

    public ArrayList<PhongBan> getListPhongBan() {
        String[] columns = new String[]{DatabaseHelper._ID, DatabaseHelper._TEN, DatabaseHelper._MOTA};
        Cursor mCursor = database.query(DatabaseHelper.TABLE_NAME_PHONG_BAN, columns, null, null, null, null, null);
        ArrayList<PhongBan> phongBans = new ArrayList<>();
        for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            PhongBan phongBan = new PhongBan();
            int id = mCursor.getInt(mCursor.getColumnIndex(DatabaseHelper._ID));
            String ten = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper._TEN));
            String mota = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper._MOTA));
            phongBan.setMa(id);
            phongBan.setMota(mota);
            phongBan.setTen(ten);
            phongBans.add(phongBan);
        }
        return phongBans;
    }

    public int updateNV(int _idNV, NhanVien nhanVien) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper._TEN, nhanVien.getTen());
        contentValues.put(DatabaseHelper._QUEQUAN, nhanVien.getQuequan());
        contentValues.put(DatabaseHelper._NGAYSINH, nhanVien.getNgaysinh());
        contentValues.put(DatabaseHelper._ID_PB, nhanVien.getIdPb());
        int i = database.update(DatabaseHelper.TABLE_NAME_NV, contentValues, DatabaseHelper._ID + " = " + _idNV, null);
        return i;
    }

    public int updatePhongBan(int _idPB, PhongBan phongBan) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper._TEN, phongBan.getTen());
        contentValues.put(DatabaseHelper._MOTA, phongBan.getMota());
        int i = database.update(DatabaseHelper.TABLE_NAME_PHONG_BAN, contentValues, DatabaseHelper._ID + " = " + _idPB, null);
        return i;
    }

    public void deleteNhanVien(int _id) {
        database.delete(DatabaseHelper.TABLE_NAME_NV, DatabaseHelper._ID + "=" + _id, null);
    }

    public void deletePhongBan(int _id) {
        database.delete(DatabaseHelper.TABLE_NAME_PHONG_BAN, DatabaseHelper._ID + "=" + _id, null);
    }

}
