package com.example.qlnhanvien;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.qlnhanvien.model.NhanVien;

public class AddEditNhanVienActivity extends AppCompatActivity implements OnClickListener {

    private Button addTodoBtn;
    private EditText editText_ten;
    private EditText edt_ngaysinh;
    private EditText edt_quequan;

    private DBManager dbManager;
    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addedit_nv);
        editText_ten = (EditText) findViewById(R.id.edt_ten);
        edt_ngaysinh = (EditText) findViewById(R.id.edt_ngaysinh);
        edt_quequan = (EditText) findViewById(R.id.edt_quequan);
        addTodoBtn = (Button) findViewById(R.id.action_nv);
        isEdit = getIntent().getBooleanExtra("IS_EDIT", false);
        if (isEdit) {
            setTitle("Chỉnh sửa Nhân Viên");
            editText_ten.setText(getIntent().getStringExtra("TEN"));
            edt_ngaysinh.setText(getIntent().getStringExtra("NGAY_SINH"));
            edt_quequan.setText(getIntent().getStringExtra("QUE_QUAN"));
            addTodoBtn.setText("Chỉnh sửa Nhân Viên");
        } else {
            setTitle("Thêm Nhân Viên");
            addTodoBtn.setText("Thêm Nhân Viên");
        }

        dbManager = new DBManager(this);
        dbManager.open();
        addTodoBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.action_nv) {
            final String ten = editText_ten.getText().toString();
            final String ngaysinh = edt_ngaysinh.getText().toString();
            final String quequan = edt_quequan.getText().toString();
            if (isEdit) {
                dbManager.updateNV(getIntent().getIntExtra("ID", -1), new NhanVien(ten, quequan, ngaysinh));
            } else {
                dbManager.insertNhanVien(new NhanVien(ten, quequan, ngaysinh));
            }
            Intent main = new Intent(AddEditNhanVienActivity.this, ListNhanVienActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(main);
        }
    }

}