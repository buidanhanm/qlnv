package com.example.qlnhanvien;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnhanvien.adapter.NhanVienAdapter;
import com.example.qlnhanvien.model.NhanVien;
import com.example.qlnhanvien.model.PhongBan;

import java.text.BreakIterator;
import java.util.ArrayList;

public class ListPhongBanNhanVienActivity extends AppCompatActivity implements NhanVienAdapter.CallBack {

    private DBManager dbManager;
    private RecyclerView recyclerView;
    private NhanVienAdapter nhanVienAdapter;
    private PhongBan phongBan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.list_pb_nv);
        setTitle("Danh sách Nhân Viên Phòng Ban");
        dbManager = new DBManager(this);
        dbManager.open();
        phongBan = (PhongBan) getIntent().getSerializableExtra("PB");

        TextView id_pb, ten_pb, mota_pb;
        id_pb = findViewById(R.id.id_pb);
        ten_pb = findViewById(R.id.ten_pb);
        mota_pb = findViewById(R.id.mota_pb);
        id_pb.setText(String.format(this.getResources().getString(R.string.id), phongBan.getMa() + ""));
        ten_pb.setText(String.format(this.getResources().getString(R.string.ten), phongBan.getTen() + ""));
        mota_pb.setText(String.format(this.getResources().getString(R.string.mota), phongBan.getMota() + ""));

        ArrayList nhanViens = dbManager.getListNhanVienByIdPhongBan(phongBan.getMa());
        recyclerView = (RecyclerView) findViewById(R.id.rvNhanVien);
        nhanVienAdapter = new NhanVienAdapter(nhanViens, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(nhanVienAdapter);
        findViewById(R.id.btn_add_nv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(ListPhongBanNhanVienActivity.this, ListNhanVienActivity.class);
                main.putExtra("IS_ADD_NV", true);
                main.putExtra("ID_PB", phongBan.getMa());
                startActivityForResult(main, 1000);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            updateListNv();
        }
    }

    @Override
    public void onClickViewOptions(TextView view, final NhanVien nhanVien) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.inflate(R.menu.main_2);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        nhanVien.setIdPb(0);
                        dbManager.updateNV(nhanVien.getMa(), nhanVien);
                        updateListNv();
                        return true;
                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }

    void updateListNv() {
        ArrayList nhanViens = dbManager.getListNhanVienByIdPhongBan(phongBan.getMa());
        nhanVienAdapter.updateList(nhanViens);
    }
}